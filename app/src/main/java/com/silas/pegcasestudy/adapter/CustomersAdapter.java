package com.silas.pegcasestudy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.silas.pegcasestudy.R;
import com.silas.pegcasestudy.model.Customer;


import java.util.List;


public class CustomersAdapter extends RecyclerView.Adapter<CustomersAdapter.CustomersItemHolder> {
    private List<Customer> healthConditionList;
    private Context context;
    Typeface typeface;


    public CustomersAdapter(List <Customer> healthConditionList, Context context) {
        super();
        this.healthConditionList = healthConditionList;
        this.context = context;
   //     typeface = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");
    }

    @NonNull
    @Override
    public CustomersItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_cardview, parent, false);
        CustomersAdapter.CustomersItemHolder viewHolder = new CustomersAdapter.CustomersItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomersItemHolder holder, int position) {
        Customer customer = healthConditionList.get(position);
        holder.customer_name.setText(customer.getName());
        holder.customer_location.setText(customer.getLocation_name());
        holder.sync_status.setText(String.valueOf(customer.getStatus()));



        if(holder.sync_status.getText().toString().equals("0")) {
            holder.side_color.setBackgroundColor(Color.parseColor("#F44336"));
        }
        else if(holder.sync_status.getText().toString().equals("1")){
            holder.side_color.setBackgroundColor(Color.parseColor("#4CAF50"));
        }
//        holder.communication_type_title.setTypeface(typeface);
       // holder.recordedDate.setTypeface(typeface);
    }

    @Override
    public int getItemCount() {
        return healthConditionList.size();
    }

    class CustomersItemHolder extends RecyclerView.ViewHolder {
        TextView customer_name;
        TextView customer_location;
        TextView sync_status;
        TextView side_color;


        private CustomersItemHolder(View itemView) {
            super(itemView);
            customer_name = itemView.findViewById(R.id.customer_name);
            customer_location = itemView.findViewById(R.id.customer_location);
            sync_status = itemView.findViewById(R.id.sync_status);
            side_color = itemView.findViewById(R.id.side_color);
        }
    }
}
