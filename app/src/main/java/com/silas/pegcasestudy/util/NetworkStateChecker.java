package com.silas.pegcasestudy.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.silas.pegcasestudy.database.DatabaseHelper;
import com.silas.pegcasestudy.activity.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;



public class NetworkStateChecker extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    private DatabaseHelper db;


    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        db = new DatabaseHelper(context);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {

                //getting all the unsynced names
                Cursor cursor = db.getUnsyncedCustomers();
                if (cursor.moveToFirst()) {
                    do {
                        //calling the method to save the unsynced name to MySQL
                        saveName(
                                cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PHONE_NUMBER)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_LOCATION)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_LOCATION_NAME)),
                                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DOB)),
                                cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATUS))
                        );
                    } while (cursor.moveToNext());
                }
            }
        }
    }

    /*
    * method taking two arguments
    * name that is to be saved and id of the name from SQLite
    * if the name is successfully sent
    * we will update the status as synced in SQLite
    * */
    private void saveName(final int id, final String name,String phone_number,String location,String location_name,String dob,int status) {
        try {
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("customer_name", name);
            jsonBody.put("phone_number", phone_number);
            jsonBody.put("dob", dob);
            jsonBody.put("location", location);

            JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, APIEndpoints.CASE_STUDY_URL, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {

                        if (response.getString("resp_code").equals("000")) {
                            //if there is a success
                            //storing the name to sqlite with status synced

                            //updating the status in sqlite
                            db.updateCustomerStatus(id, MainActivity.NAME_SYNCED_WITH_SERVER);

                            //sending the broadcast to refresh the list
                            context.sendBroadcast(new Intent(MainActivity.DATA_SAVED_BROADCAST));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                }

            });
            Log.d("REQUEST", String.valueOf(jsonOblect));
            VolleySingleton.getInstance(context).addToRequestQueue(jsonOblect);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
