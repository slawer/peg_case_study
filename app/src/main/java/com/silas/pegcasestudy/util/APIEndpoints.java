package com.silas.pegcasestudy.util;

public interface APIEndpoints {

    String SERVER_IP = "http://174.138.6.99:3002";

    String CASE_STUDY_URL = SERVER_IP + "/case_study_request";
}
