package com.silas.pegcasestudy.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.silas.pegcasestudy.R;
import com.silas.pegcasestudy.adapter.CustomersAdapter;
import com.silas.pegcasestudy.database.DatabaseHelper;
import com.silas.pegcasestudy.model.Customer;
import com.silas.pegcasestudy.util.APIEndpoints;
import com.silas.pegcasestudy.util.NetworkStateChecker;
import com.silas.pegcasestudy.util.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    // EditText edtDob;

    //database helper object
    private DatabaseHelper db;

    //View objects
    private Button buttonSave;
    private Button buttonCancel;
    private FloatingActionButton fabAddNewCustomer;

    private EditText edtphone_number;
    private EditText edtDob;
    private EditText edtFullname;
    private EditText edtLocation;
    private RecyclerView recyclerCustomers;

    //List to store all the names
    private ArrayList<Customer> customer;

    //1 means data is synced and 0 means data is not synced
    public static final int NAME_SYNCED_WITH_SERVER = 1;
    public static final int NAME_NOT_SYNCED_WITH_SERVER = 0;

    //a broadcast to know weather the data is synced or not
    public static final String DATA_SAVED_BROADCAST = "net.simplifiedcoding.datasaved";

    //Broadcast receiver to know the sync status
    private BroadcastReceiver broadcastReceiver;

    //adapterobject for list view
    private CustomersAdapter nameAdapter;

    private static final String TAG = "MainDOB";
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    SharedPreferences prefs;
    SharedPreferences.Editor edit;

    String coordinaties;

    ScrollView scrollView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        edtphone_number = findViewById(R.id.phone_number);
        edtDob = findViewById(R.id.date_of_birth);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();


        registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


        db = new DatabaseHelper(this);
        customer = new ArrayList<>();

        nameAdapter = new CustomersAdapter(customer, this);

        buttonSave = (Button) findViewById(R.id.btnSave);
        scrollView2=findViewById(R.id.scrollView2);
        fabAddNewCustomer= findViewById(R.id.fabAddCustomer);
        buttonCancel = (Button) findViewById(R.id.btnCancel);
        edtFullname = (EditText) findViewById(R.id.full_name);
        edtLocation = (EditText) findViewById(R.id.customer_location);


        recyclerCustomers = findViewById(R.id.recyclerCustomers);

        recyclerCustomers.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerCustomers.setLayoutManager(llm);

        //recyclerInventory.setAdapter(resultInventoryAdapter);
        recyclerCustomers.setAdapter(nameAdapter);

        //adding click listener to button
        buttonSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);

        //calling the method to load all the stored names
        loadNames();

        //the broadcast receiver to update sync status
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //loading the names again
                loadNames();
            }
        };

        //registering the broadcast receiver to update sync status
        registerReceiver(broadcastReceiver, new IntentFilter(DATA_SAVED_BROADCAST));


        edtDob.setOnClickListener(this);
        edtLocation.setOnClickListener(this);
        fabAddNewCustomer.setOnClickListener(this);

        onDateSetListener = (datePicker, year, month, day) -> {
            month = month + 1;
            Log.d(TAG, "onDateSet: dd/mm/yyyy" + day + "/" + month + "/" + year);

            String date = day + "/" + month + "/" + year;
            edtDob.setText(date);
        };

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.date_of_birth:
                getDOB();
                break;
            case R.id.btnSave:
                saveNameToServer();
                break;

            case R.id.customer_location:
                startActivity(new Intent(this, MapsActivity.class));
                break;

            case R.id.fabAddCustomer:
                scrollView2.setVisibility(View.VISIBLE);
                fabAddNewCustomer.setVisibility(View.GONE);
                recyclerCustomers.setVisibility(View.GONE);
                break;

            case R.id.btnCancel:
                scrollView2.setVisibility(View.GONE);
                fabAddNewCustomer.setVisibility(View.VISIBLE);
                recyclerCustomers.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtLocation.setText(prefs.getString("location_name", ""));
        coordinaties = prefs.getString("location_coordinates", "");
    }

    public void getDOB() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, onDateSetListener, year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dialog.show();
    }

    /*
     * this method will
     * load the names from the database
     * with updated sync status
     * */
    private void loadNames() {
        customer.clear();
        Cursor cursor = db.getCustomers();
        if (cursor.moveToFirst()) {
            do {
                Customer n = new Customer(
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PHONE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_LOCATION)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_LOCATION_NAME)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DOB)),
                        cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATUS))
                );
                customer.add(n);
                //refreshList();
            } while (cursor.moveToNext());
        }
        refreshList();

        Log.d("CUR", String.valueOf(cursor));


    }

    /*
     * this method will simply refresh the list
     * */
    private void refreshList() {
        nameAdapter.notifyDataSetChanged();
    }


    /*
     * this method is saving the name to ther server
     * */
    private void saveNameToServer() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        final String name = edtFullname.getText().toString().trim();
        final String dob = edtDob.getText().toString().trim();
        final String phone_number = edtphone_number.getText().toString().trim();
        final String location = coordinaties.trim();
        final String location_name = edtLocation.getText().toString().trim();




        if (!name.isEmpty() || !dob.isEmpty() || !location.isEmpty() || !phone_number.isEmpty()) {
            // final String name = editTextName.getText().toString().trim();

//
            try {
                JSONObject jsonBody = new JSONObject();

                jsonBody.put("customer_name", name);
                jsonBody.put("phone_number", phone_number);
                jsonBody.put("dob", dob);
                jsonBody.put("location", location);

                Log.d("OBJS", String.valueOf(jsonBody));

                JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, APIEndpoints.CASE_STUDY_URL, jsonBody, response -> {
                    progressDialog.dismiss();
                    //Toast.makeText(getApplicationContext(), "Response:  " + response.toString(), Toast.LENGTH_SHORT).show();
                    try {

                        if (response.getString("resp_code").equals("000")) {
                            //if there is a success
                            //storing the name to sqlite with status synced

                            saveNameToLocalStorage(name, phone_number, dob, location, location_name, NAME_SYNCED_WITH_SERVER);
                            Toast.makeText(MainActivity.this, response.getString("resp_desc"), Toast.LENGTH_SHORT).show();
                        } else {
                            //if there is some error
                            //saving the name to sqlite with status unsynced
                            Toast.makeText(MainActivity.this, "Not Successful", Toast.LENGTH_SHORT).show();
                            saveNameToLocalStorage(name, phone_number, dob, location, location_name, NAME_NOT_SYNCED_WITH_SERVER);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                    progressDialog.dismiss();
                    //onBackPressed();
                    saveNameToLocalStorage(name, phone_number, dob, location, location_name, NAME_NOT_SYNCED_WITH_SERVER);

                });
                Log.d("REQUEST", String.valueOf(jsonOblect));
                VolleySingleton.getInstance(this).addToRequestQueue(jsonOblect);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "All fields are required", Toast.LENGTH_SHORT).show();
        }


    }

    //saving the name to local storage
    @SuppressLint("RestrictedApi")
    private void saveNameToLocalStorage(String name, String phone_number, String dob, String location, String locationName, int status) {
        edtFullname.setText("");
        edtDob.setText("");
        coordinaties = "";
        edtLocation.setText("");
        edtphone_number.setText("");
        edit.putString("location_name", "");
        edit.putString("location_coordinates", "");
        edit.commit();
        recyclerCustomers.setVisibility(View.VISIBLE);
        fabAddNewCustomer.setVisibility(View.VISIBLE);
        scrollView2.setVisibility(View.GONE);
        db.addCustomer(name, phone_number, location, locationName, dob, status);
        Customer n = new Customer(name, phone_number, location, locationName, dob, status);
        customer.add(n);
        refreshList();
    }

}
