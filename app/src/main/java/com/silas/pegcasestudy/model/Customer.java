package com.silas.pegcasestudy.model;



public class Customer {
    private String name;
    private String phone_number;
    private String location;
    private String location_name;
    private String dob;
    private int status;

    public Customer(String name, String phone_number, String location, String location_name, String dob, int status) {
        this.name = name;
        this.phone_number = phone_number;
        this.location = location;
        this.location_name = location_name;
        this.dob = dob;
        this.status = status;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getLocation() {
        return location;
    }

    public String getLocation_name() {
        return location_name;
    }

    public String getDob() {
        return dob;
    }

    public String getName() {
        return name;
    }

    public int getStatus() {
        return status;
    }
}
